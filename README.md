This repository contains git-chat, the worlds silliest chat program (as far as I know). The only upside of this is you can hide a chat inside your git repo.

# Getting started
* Place `git-chat` onto your path
* Place `notify` onto your path
* Open any git repository
* Run `git chat --init`
* Run `git chat [message]`
* ???
* Profit?

# Known Issues
* You need to have origin pointing at a machine that has also ran `git chat --init`
* `notify` only works on windows, feel free to monkey patch the script to work on other things

# Planned feature
* Customisation over the origin that is used
* Customisation for where chat branches are stored
* Pulling messages instead of just relying on pushes
* Making the notification open a log when clicked
* Some how auto forming a mesh network
* Migrating off git?
